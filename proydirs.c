#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<dirent.h>
#include<libgen.h>
#include<fcntl.h>
#include<unistd.h>
void cifra(char *archivo){
	int fd,ftmp; //Declaracion de un descriptor de archivos.
	char *temp="temp";
	fd=open(archivo,O_RDWR,S_IRUSR|S_IWUSR);
	ftmp=open(temp,O_WRONLY|O_CREAT|O_APPEND|O_TRUNC,S_IRUSR|S_IWUSR);
		
	char buff;
	int rr;
	rr=read(fd,&buff,1);
	while(rr>0){
		write(ftmp,&buff,1);
		rr=read(fd,&buff,1);
	}
	close(ftmp);	
	close(fd);
	
	fd=open(archivo,O_WRONLY|O_TRUNC|O_APPEND,S_IRUSR|S_IWUSR);
	ftmp=open(temp,O_RDONLY,S_IRUSR);
	
	rr=read(ftmp,&buff,1);
	while(rr>0){
		buff = buff ^ 0x10101010;
		write(fd,&buff,1);
		rr=read(ftmp,&buff,1);
	}
	close(ftmp);
	close(fd);
	
	ftmp=open(temp,O_WRONLY|O_CREAT|O_APPEND|O_TRUNC,S_IRUSR|S_IWUSR);
	close(ftmp);
}

int main(int argc, char *argv[]){
	if(argc<2){
		perror("Se necesita un argumento\n");
		return(1111);
	}
	DIR * dir;
	struct dirent *dp;	//Estructura de directorios
	
	dir=opendir(argv[1]);
	dp=readdir(dir);
	while(dp){
		cifra(dp->d_name);
		dp=readdir(dir);
	}
}
